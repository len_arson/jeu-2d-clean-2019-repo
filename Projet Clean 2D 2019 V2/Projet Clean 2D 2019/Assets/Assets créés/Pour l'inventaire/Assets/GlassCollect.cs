﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassCollect : MonoBehaviour
{
    public GameObject ActionText;
    bool PlayerIsNear;

    void OnEnable()
    {
        ActionText.SetActive(false);
    }

    void Update()
    {
        if (PlayerIsNear && Input.GetKeyDown(KeyCode.E))
        {
            // pickup clef
            gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerIsNear = true;
            ActionText.SetActive(true);

        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerIsNear = false;
            ActionText.SetActive(false);

        }
    }

}
