﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour
{
    public GameObject ActionText;
    bool PlayerIsNear;
    public string SceneName;

    // Start is called before the first frame update
    void OnEnable()
    {
        ActionText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerIsNear && Input.GetKeyDown(KeyCode.E))
        {
            //Faire l'action
            SceneManager.LoadScene(SceneName);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PlayerIsNear = true;
            ActionText.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PlayerIsNear = false;
            ActionText.SetActive(false);
        }
    }
}
