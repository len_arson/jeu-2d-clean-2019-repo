﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {


    private Rigidbody2D rb;

    [SerializeField]
    private float movementSpeed;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
     void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");


        HandleMovement(horizontal);
    }
    private void HandleMovement(float horizontal)
    {
        rb.velocity = new Vector2(horizontal * movementSpeed, rb.velocity.y);
    }
}

//https://www.youtube.com/watch?v=HpJMhNmpIxY
