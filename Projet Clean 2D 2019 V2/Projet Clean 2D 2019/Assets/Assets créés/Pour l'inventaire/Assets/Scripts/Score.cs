﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text countText; //nombre d'indice récolté

    private int count; //score

    void Start()
    {
        count = 0; // initialize score
    }

    void SetCountText()
    {
        countText.text = "Indices : " + count.ToString();
        Debug.Log("Hello");
    }
}
