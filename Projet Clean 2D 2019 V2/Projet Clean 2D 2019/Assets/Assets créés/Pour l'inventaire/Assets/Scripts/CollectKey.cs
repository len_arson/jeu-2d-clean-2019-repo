﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectKey : MonoBehaviour
{
    public GameObject ActionText;
    bool PlayerIsNear;
    public Text countText;

    private int count;

    void Start()
    {
        count = 0;
    }

    void OnEnable()
    {
        ActionText.SetActive(false);
    }

    void Update()
    {
        if (PlayerIsNear && Input.GetKeyDown(KeyCode.E))
        {
            // pickup clef
            gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
       if (collision.gameObject.CompareTag("Player"))
        {
            PlayerIsNear = true;
            ActionText.SetActive(true);

            count = count + 1;

            SetCountText();

        } 
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerIsNear = false;
            ActionText.SetActive(false);

        }
    }

    void SetCountText()
    {
        countText.text = "Indices : " + count.ToString();
    }
}
