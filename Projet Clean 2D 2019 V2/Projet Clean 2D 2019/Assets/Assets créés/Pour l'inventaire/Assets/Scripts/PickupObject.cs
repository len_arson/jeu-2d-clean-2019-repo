﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupObject : MonoBehaviour
{
    public string ID;
    //recuperer l'icone et son image
    public Sprite Icon
    {
        get { return GetComponent<SpriteRenderer>().sprite; }
    }
    public string Description;
    
    public GameObject ActionText;
    bool PlayerIsNear;
    
    void OnEnable()
    {
        ActionText.SetActive(false);
    }

    void Update()
    {
        if (PlayerIsNear && Input.GetKeyDown(KeyCode.E))
        {
            // pickup object
            gameObject.SetActive(false);
            Inventory.AddItem(this);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerIsNear = true;
            ActionText.SetActive(true);
            
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerIsNear = false;
            ActionText.SetActive(false);

        }
    }
    
}
