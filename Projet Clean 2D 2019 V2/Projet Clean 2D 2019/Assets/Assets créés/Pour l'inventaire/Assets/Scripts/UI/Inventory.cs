﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Inventory : MonoBehaviour
{
    static Dictionary<string, ObjectInfo> Items = new Dictionary<string, ObjectInfo>();
    List<ObjectInfo> _itemsOfButton = new List<ObjectInfo>();

    public Transform Slots;
    public TextMeshProUGUI Description;
    
    // Start is called before the first frame update
    void OnEnable()
    {
        var i = 0;
        _itemsOfButton.Clear();

        foreach (var item in Items)
        {
            var slot = Slots.GetChild(i);
            slot.GetComponent<Image>().sprite = item.Value.Icon;
            _itemsOfButton.Add(item.Value);
            i++;
        }
    }
    
    public void SelectItem(int id)
    {
        Description.text = _itemsOfButton[id].Description;
    }

    public static void AddItem(PickupObject obj)
    {
        ObjectInfo info = null;

        if (!Items.TryGetValue(obj.ID, out info)) {
            info = new ObjectInfo(obj);
            Items.Add(obj.ID, info);
        }

        info.Count++;
    }

    public class ObjectInfo
    {
        public string Description;
        public Sprite Icon;
        public int Count;

        public ObjectInfo(PickupObject obj)
        {
            Description = obj.Description;
            Icon = obj.Icon;
        }
    }
}
